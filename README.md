Introduction
============

Open ChatBot is an [Alliance For Open ChatBot](https://www.alliance-open-chatbot.org) initiative that propose a common API specification for a simple chatbot conversation (text and/or voice). The aim of the alliance is to define a collaborative and open communication standard between chatbots.

Current document is the very first **DRAFT** of this communication standard API.  

As BitBucket does not support asciidoc, Draft specifications is currently hosted [here](http://openchatbot.io/draft).

_Note: Source file is [draft.adoc](https://bitbucket.org/allianceopenchatbot/framework/src/master/draft.adoc)_
